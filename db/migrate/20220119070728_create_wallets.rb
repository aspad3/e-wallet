class CreateWallets < ActiveRecord::Migration[6.0]
  def change
    create_table :wallets do |t|
      t.references :user
      t.string :address

      t.timestamps
    end
  end
end
