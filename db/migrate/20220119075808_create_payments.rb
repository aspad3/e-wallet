class CreatePayments < ActiveRecord::Migration[6.0]
  def change
    create_table :payments do |t|
      t.bigint :account_number
      t.string :account_name
      t.date :payment_date
      t.timestamps
    end
  end
end
