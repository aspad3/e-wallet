class CreateTransactions < ActiveRecord::Migration[6.0]
  def change
    create_table :transactions do |t|
      t.references :wallet
      t.references :payment
      t.string :transaction_type
      t.date :date
      t.decimal :price
      t.boolean :status, default: false
    end
  end
end
