class CreateTransactionHistories < ActiveRecord::Migration[6.0]
  def change
    create_table :transaction_histories do |t|
      t.references :transaction
      t.references :payment
      t.decimal :price
      t.date :date
    end
  end
end
