Rails.application.routes.draw do
  get 'transaction/index'
  get 'transfer/index'
  post 'transfer/create'

  get 'withdraw/index'
  post 'withdraw/create'

  get 'topups/index'
  post 'topups/create'

  resources :accounts
  devise_for :users
  get 'home/index'
  root to: "home#index"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
