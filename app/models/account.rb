class Account < ApplicationRecord
  belongs_to :user

  validates :name, :number, presence: true
  validates_uniqueness_of :number
  validates :number, length: { minimum: 6, maximum: 16 }

  def full_name
    "#{name}-#{number}"
  end
end
