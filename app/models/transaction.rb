class Transaction < ApplicationRecord
  validate :check_wallet_user
  validates_numericality_of :price, greater_than_or_equal_to: 10000
  after_create :generate_history
  belongs_to :payment

  def check_wallet_user
    return if self.transaction_type == 'debit'

    current_wallet = Wallet.find(self.wallet_id)
    status_value = current_wallet.my_value - self.price.to_f < 0
    errors.add(:price, "Wallet not enough") if status_value
  end

  def generate_history
    TransactionHistory.create(
      transaction_id: self.id,
      payment_id:  self.payment_id,
      price: self.price,
      date: DateTime.now.strftime("%Y-%m-%d")
    )
  end

  def format_price
    ActionController::Base.helpers.number_to_currency(price, unit: 'Rp ', precision: 2)
  end
end
