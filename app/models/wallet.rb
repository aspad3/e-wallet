class Wallet < ApplicationRecord
  belongs_to :user

  def my_value
    debit = Transaction.where(transaction_type: "debit", wallet_id: id).sum("price")
    credit = Transaction.where(transaction_type: "credit", wallet_id: id).sum("price")
    total =  debit - credit
    total
  end

  def my_cash
    ActionController::Base.helpers.number_to_currency(my_value, unit: '', precision: 2)
  end
end
