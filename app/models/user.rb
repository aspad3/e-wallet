class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

   after_create :generate_wallet

  has_one :wallet

  def generate_wallet
    dwall = Wallet.new
    dwall.address =  SecureRandom.base64(16)
    dwall.user_id = self.id
    dwall.save
  end

  def full_address
    "#{email} #{wallet.address}"
  end
end
