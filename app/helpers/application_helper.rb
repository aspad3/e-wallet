module ApplicationHelper
  def bank_list
    ['Bank Mandiri',
     'Bank Rakyat Indonesia (BRI)',
     'Bank Central Asia (BCA)',
     'Bank Negara Indonesia (BNI)',
     'Bank Tabungan Negara (BTN)',
     'Bank CIMB Niaga',
     'Bank OCBC NISP.',
     'Panin Bank']
  end
end
