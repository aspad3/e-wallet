class BaseController < ApplicationController
  before_action :authenticate_user!
  layout 'admin_lte_2'

  def custom_error(data)
    data.errors.messages.map { |_key, message| message.last.to_s.titleize }.join(', ')
  rescue StandardError
    "Failed #{data.errors.messages}"
  end
end
