class TransactionController < BaseController
  def index
    @transactions = Transaction.where(wallet_id: current_user.wallet.id).order('id desc')
  end
end
