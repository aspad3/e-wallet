class WithdrawController < BaseController
  def index
    @accounts = Account.where(user_id: current_user.id)
  end

  def create
    @account = Account.find_by_id(params[:account_id])
    return redirect_to '/withdraw/index', notice: 'please select account' if @account.nil?

    params[:payment][:account_number] = @account.number
    params[:payment][:account_name] = @account.name
    @payment = Payment.new(payment_params)

    respond_to do |format|
      if @payment.save
        format.html { redirect_to '/withdraw/index', notice: 'Withdraw was successfully created' }
      else
        format.html { redirect_to request.referer, notice: custom_error(@payment) }
      end
    end
  end

  def payment_params
    params.require(:payment).permit(:account_number, :account_name, transactions_attributes: [:price, :transaction_type, :date, :status, :wallet_id])
  end
end
