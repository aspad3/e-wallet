class TransferController < BaseController
  def index
    @users = User.where.not(id: current_user.id)
  end

  def create
    @user = User.find_by_id(params[:user_id])
    return request.referer, notice: 'please select user' if @user.nil?

    params[:payment][:account_number] = @user.id
    params[:payment][:account_name] = @user.wallet.address
    @payment = Payment.new(payment_params)

    respond_to do |format|
      if @payment.save
        format.html { redirect_to '/transfer/index', notice: 'Transfer was successfully created' }
      else
        format.html { redirect_to request.referer, notice: custom_error(@payment) }
      end
    end
  end

  def payment_params
    params.require(:payment).permit(:account_number, :account_name, transactions_attributes: [:price, :transaction_type, :date, :status, :wallet_id])
  end
end
