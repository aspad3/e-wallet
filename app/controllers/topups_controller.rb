class TopupsController < BaseController
  def index
  end

  def create
    @payment = Payment.new(payment_params)

    respond_to do |format|
      if @payment.save
        format.html { redirect_to '/topups/index', notice: 'Topup was successfully created' }
      else
        format.html { redirect_to request.referer, notice: custom_error(@payment) }
      end
    end
  end

  private

  def payment_params
    params.require(:payment).permit(:account_number, :account_name, transactions_attributes: [:price, :transaction_type, :date, :status, :wallet_id])
  end
end
